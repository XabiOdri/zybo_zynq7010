/*
 * leds.c
 *
 *  Created on: 29 oct. 2017
 *      Author: xabie
 */

#include "axi_config.h"
#include <xgpiops.h>

#include "leds.h"


static XGpioPs Gpio;


void init_led(void)
{

	XGpioPs_Config *ConfigPtr;

	ConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
	XGpioPs_CfgInitialize(&Gpio, ConfigPtr, ConfigPtr->BaseAddr);
	XGpioPs_SetOutputEnablePin(&Gpio,LED4,OUT);
	XGpioPs_SetDirectionPin(&Gpio,LED4,OUT);

	XGpioPs_SetOutputEnablePin(&Gpio,LED0,OUT);
	XGpioPs_SetDirectionPin(&Gpio,LED0,OUT);

	//XGpio_Initialize(&led, XPAR_GPIO_0_DEVICE_ID); //LED
	//XGpio_SetDataDirection(&led, LED_CHANNEL, 0x0);
}

void turnON_led(int led)
{
	XGpioPs_WritePin(&Gpio,led,1);
}

void turnOFF_led(int led)
{
	XGpioPs_WritePin(&Gpio,led,0);
}

void setLED(int led, int mode)
{
	XGpioPs_WritePin(&Gpio,led,mode);
}

