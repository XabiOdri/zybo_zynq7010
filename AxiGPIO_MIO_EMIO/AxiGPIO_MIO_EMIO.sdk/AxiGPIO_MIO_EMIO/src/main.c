/*
 * main.c
 *
 *  Created on: 31 oct. 2017
 *      Author: xabie
 */

#include <stdio.h>

#include "axi_config.h"
#include "leds.h"
#include "buttons.h"

#define ever	(;;)

void task1(void);
void task2(void);
void task3(void);
void task4(void);

void task1(void)
{
	//Leer 2 botones BTN4 y BTN5 (MIO) y sacar la operacion OR de los dos en el LD0 EMIO.
	setLED(LED0, (getButtonStatus(BTN4))^(getButtonStatus(BTN5)));
}

void task2(void)
{
	//Leer 1 bot�n BTN0 (EMIO) y sacar su valor en el LD4 (MIO)
	setLED(LED4,getButtonStatus(BTN0));
}

void task3(void)
{
	//Leer bot�n BTN3 y sacar su valor en LD3 utilizando AXI_GPIO
	setLED_AXI(LED3 ,getButtonStatusAXI(BTN3));
}

void task4(void)
{
	//Ense�ar por el puerto serie (UART) el valor de los botones BTN4 y BTN5
	int status_btn4, status_btn5;
	static int last_status_btn4 = 0, last_status_btn5 = 0;

	status_btn4 = getButtonStatus(BTN4);
	status_btn5 = getButtonStatus(BTN5);

	if( (status_btn4!=last_status_btn4) || (status_btn5!=last_status_btn5) )
	{
		printf("\x1B[H");		//Set cursor to top left of terminal
		printf("\x1B[2J");	//Clear terminal
		printf("BTN4 => %i /// BTN5 => %i\n", status_btn4, status_btn5);

		last_status_btn4 = status_btn4;
		last_status_btn5 = status_btn5;
	}
}

int main(void)
{
	init_led();
	initButtons();
	init_gpio_axi();

	for ever
	{
		task1();
		task2();
		task3();
		task4();
	}
	return 0;
}
