/*
 * axi_config.h
 *
 *  Created on: 31 oct. 2017
 *      Author: xabie
 */

#ifndef SRC_AXI_CONFIG_H_
#define SRC_AXI_CONFIG_H_

#define AXI_BTN_channel 1
#define AXI_LED_channel 2

#define AXI_IN	1
#define AXI_OUT	0

#define BTN3	0
#define LED3	0

void init_gpio_axi(void);
int getButtonStatusAXI(int btn);
void setLED_AXI(int led, int mode);

#endif /* SRC_AXI_CONFIG_H_ */
