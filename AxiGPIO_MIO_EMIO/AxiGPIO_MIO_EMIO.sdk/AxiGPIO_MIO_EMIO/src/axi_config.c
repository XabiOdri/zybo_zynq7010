/*
 * axi_config.c
 *
 *  Created on: 1 nov. 2017
 *      Author: xabie
 */

#include <xgpio.h>
#include "axi_config.h"

XGpio axi_gpio;

void init_gpio_axi(void)
{

	XGpio_Initialize(&axi_gpio, XPAR_GPIO_0_DEVICE_ID);

	XGpio_SetDataDirection(&axi_gpio, AXI_BTN_channel, AXI_IN);
	XGpio_SetDataDirection(&axi_gpio, AXI_LED_channel, AXI_OUT);
}

int getButtonStatusAXI(int btn)
{
	return (XGpio_DiscreteRead(&axi_gpio, AXI_BTN_channel)) & (1<<btn);
}

void setLED_AXI(int led, int mode)
{
	if(mode==0)
	{
		XGpio_DiscreteClear(&axi_gpio, AXI_LED_channel, 1<<LED3);
	}
	else
	{
		XGpio_DiscreteSet(&axi_gpio, AXI_LED_channel, 1<<LED3);
	}
}
